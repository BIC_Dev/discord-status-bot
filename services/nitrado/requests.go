package nitrado

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/discord-status-bot/utils"
)

// GetServerStatus func
func (request *Request) GetServerStatus(serviceToken string, serverID string, nitradoToken string) (GameserverResponse, *utils.ModelError) {
	httpClient := &http.Client{
		Timeout: time.Second * 60,
	}

	url := request.Config.NitradoService.BaseURL + "/gameserver/" + serverID

	newRequest, newRequestErr := http.NewRequest(http.MethodGet, url, nil)

	if newRequestErr != nil {
		modelErr := utils.NewModelError(newRequestErr)
		modelErr.SetMessage("Failed to create new request to GET nitrado gameserver")
		modelErr.SetStatus(http.StatusInternalServerError)
		return GameserverResponse{}, modelErr
	}

	newRequest.Header.Add("Service-Token", serviceToken)
	newRequest.Header.Add("Account-Name", nitradoToken)

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		modelErr := utils.NewModelError(requestErr)
		modelErr.SetMessage("Failed to send request to GET nitrado gameserver")
		modelErr.SetStatus(http.StatusBadRequest)
		return GameserverResponse{}, modelErr
	}

	switch response.StatusCode {
	case http.StatusOK:
		var responseStruct GameserverResponse
		jsonErr := json.NewDecoder(response.Body).Decode(&responseStruct)

		if jsonErr != nil {
			modelErr := utils.NewModelError(jsonErr)
			modelErr.SetMessage("Bad response data from GET nitrado gameserver")
			modelErr.SetStatus(response.StatusCode)
			return GameserverResponse{}, modelErr
		}

		return responseStruct, nil
	}

	modelErr := utils.NewModelError(fmt.Errorf("Failure response from GET nitrado gameserver status code (%d)", response.StatusCode))
	modelErr.SetMessage("Bad response from GET nitrado gameserver")
	modelErr.SetStatus(response.StatusCode)
	return GameserverResponse{}, modelErr
}

// GetAllServices func
func (request *Request) GetAllServices(serviceToken string, nitradoToken string) (ServicesResponse, *utils.ModelError) {
	httpClient := &http.Client{
		Timeout: time.Second * 60,
	}

	url := request.Config.NitradoService.BaseURL + "/service"

	newRequest, newRequestErr := http.NewRequest(http.MethodGet, url, nil)

	if newRequestErr != nil {
		modelErr := utils.NewModelError(newRequestErr)
		modelErr.SetMessage("Failed to create new request to GET all nitrado services")
		modelErr.SetStatus(http.StatusInternalServerError)
		return ServicesResponse{}, modelErr
	}

	newRequest.Header.Add("Service-Token", serviceToken)
	newRequest.Header.Add("Account-Name", nitradoToken)

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		modelErr := utils.NewModelError(requestErr)
		modelErr.SetMessage("Failed to send request to GET all nitrado services")
		modelErr.SetStatus(http.StatusBadRequest)
		return ServicesResponse{}, modelErr
	}

	switch response.StatusCode {
	case http.StatusOK:
		var responseStruct ServicesResponse
		jsonErr := json.NewDecoder(response.Body).Decode(&responseStruct)

		if jsonErr != nil {
			modelErr := utils.NewModelError(jsonErr)
			modelErr.SetMessage("Bad response data from GET all nitrado services")
			modelErr.SetStatus(response.StatusCode)
			return ServicesResponse{}, modelErr
		}

		return responseStruct, nil
	}

	modelErr := utils.NewModelError(fmt.Errorf("Failure response from GET all nitrado services status code (%d)", response.StatusCode))
	modelErr.SetMessage("Bad response from GET all nitrado services")
	modelErr.SetStatus(response.StatusCode)
	return ServicesResponse{}, modelErr
}

// GetService func
func (request *Request) GetService(serviceToken string, serverID string, nitradoToken string) (ServicesResponse, *utils.ModelError) {
	httpClient := &http.Client{
		Timeout: time.Second * 60,
	}

	url := request.Config.NitradoService.BaseURL + "/service/" + serverID

	newRequest, newRequestErr := http.NewRequest(http.MethodGet, url, nil)

	if newRequestErr != nil {
		modelErr := utils.NewModelError(newRequestErr)
		modelErr.SetMessage("Failed to create new request to GET nitrado services")
		modelErr.SetStatus(http.StatusInternalServerError)
		return ServicesResponse{}, modelErr
	}

	newRequest.Header.Add("Service-Token", serviceToken)
	newRequest.Header.Add("Account-Name", nitradoToken)

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		modelErr := utils.NewModelError(requestErr)
		modelErr.SetMessage("Failed to send request to GET nitrado service")
		modelErr.SetStatus(http.StatusBadRequest)
		return ServicesResponse{}, modelErr
	}

	switch response.StatusCode {
	case http.StatusOK:
		var responseStruct ServicesResponse
		jsonErr := json.NewDecoder(response.Body).Decode(&responseStruct)

		if jsonErr != nil {
			modelErr := utils.NewModelError(jsonErr)
			modelErr.SetMessage("Bad response data from GET nitrado service")
			modelErr.SetStatus(response.StatusCode)
			return ServicesResponse{}, modelErr
		}

		return responseStruct, nil
	}

	modelErr := utils.NewModelError(fmt.Errorf("Failure response from GET nitrado service status code (%d)", response.StatusCode))
	modelErr.SetMessage("Bad response from GET nitrado service")
	modelErr.SetStatus(response.StatusCode)
	return ServicesResponse{}, modelErr
}
