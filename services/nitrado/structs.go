package nitrado

import "gitlab.com/BIC_Dev/discord-status-bot/utils"

// Request struct
type Request struct {
	Config *utils.Config
	Log    *utils.Log
}

// GameserverResponse struct
type GameserverResponse struct {
	Gameserver Gameserver `json:"gameserver"`
}

// Gameserver struct
type Gameserver struct {
	Status           string      `json:"status"`
	LastStatusChange int         `json:"last_status_change"`
	MustBeStarted    bool        `json:"must_be_started"`
	WebsocketToken   string      `json:"websocket_token"`
	Username         string      `json:"username"`
	UserID           int         `json:"user_id"`
	ServiceID        int         `json:"service_id"`
	LocationID       int         `json:"location_id"`
	MinecraftMode    bool        `json:"minecraft_mode"`
	IP               string      `json:"ip"`
	Ipv6             interface{} `json:"ipv6"`
	Port             int         `json:"port"`
	QueryPort        int         `json:"query_port"`
	RconPort         int         `json:"rcon_port"`
	Label            string      `json:"label"`
	Type             string      `json:"type"`
	Memory           string      `json:"memory"`
	MemoryMb         int         `json:"memory_mb"`
	Game             string      `json:"game"`
	GameHuman        string      `json:"game_human"`
	Slots            int         `json:"slots"`
	Query            Query       `json:"query"`
	SuspendDate      int64       `json:"suspend_date"`
	DeleteDate       int64       `json:"delete_date"`
}

// Query struct
type Query struct {
	ServerName    string `json:"server_name"`
	ConnectIP     string `json:"connect_ip"`
	Map           string `json:"map"`
	Version       string `json:"version"`
	PlayerCurrent int    `json:"player_current"`
	PlayerMax     int    `json:"player_max"`
	Players       []struct {
		ID     int    `json:"id"`
		Name   string `json:"name"`
		Bot    bool   `json:"bot"`
		Score  int    `json:"score"`
		Frags  int    `json:"frags"`
		Deaths int    `json:"deaths"`
		Time   int    `json:"time"`
		Ping   int    `json:"ping"`
	} `json:"players"`
}

// ServicesResponse struct
type ServicesResponse struct {
	Services []Service `json:"services"`
}

// ServiceResponse struct
type ServiceResponse struct {
	Service Service `json:"service"`
}

// Service struct
type Service struct {
	ID                    int    `json:"id"`
	LocationID            int    `json:"location_id"`
	Status                string `json:"status"`
	WebsocketToken        string `json:"websocket_token"`
	UserID                int    `json:"user_id"`
	Comment               string `json:"comment"`
	AutoExtension         bool   `json:"auto_extension"`
	AutoExtensionDuration int    `json:"auto_extension_duration"`
	Type                  string `json:"type"`
	TypeHuman             string `json:"type_human"`
	Details               struct {
		Address       string `json:"address"`
		Name          string `json:"name"`
		Game          string `json:"game"`
		PortlistShort string `json:"portlist_short"`
		FolderShort   string `json:"folder_short"`
		Slots         int    `json:"slots"`
	} `json:"details"`
	StartDate    string   `json:"start_date"`
	SuspendDate  string   `json:"suspend_date"`
	DeleteDate   string   `json:"delete_date"`
	SuspendingIn int      `json:"suspending_in"`
	DeletingIn   int      `json:"deleting_in"`
	Username     string   `json:"username"`
	Roles        []string `json:"roles"`
}
