package discord

import (
	"fmt"
	"math"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/discord-status-bot/models"
	"gitlab.com/BIC_Dev/discord-status-bot/utils"
	"gitlab.com/BIC_Dev/discord-status-bot/utils/db"
)

// PostStatus func
func PostStatus(categoryStatuses *CategoryStatuses, dgs *discordgo.Session, c *utils.Config) error {
	embed := generateEmbed(categoryStatuses, c)
	statusChannel := strconv.FormatInt(categoryStatuses.Category.Metadata.StatusChannel, 10)

	dbMessage, dbMessageErr := getMessage(categoryStatuses, c)

	if dbMessageErr != nil || dbMessage.DiscordMessageID == 0 {
		err := CreateNewStatusPost(embed, categoryStatuses, dgs, c)

		return err
	}

	messageID := strconv.FormatInt(dbMessage.DiscordMessageID, 10)
	messageEdit := discordgo.NewMessageEdit(statusChannel, messageID)
	messageEdit.SetEmbed(embed)
	_, err := dgs.ChannelMessageEditComplex(messageEdit)

	// Determine if message no longer exists error and do message create instead

	if err != nil {
		if strings.Contains(err.Error(), "10008") {
			err := CreateNewStatusPost(embed, categoryStatuses, dgs, c)
			return err
		}

		return err
	}

	return nil
}

// CreateNewStatusPost func
func CreateNewStatusPost(embed *discordgo.MessageEmbed, categoryStatuses *CategoryStatuses, dgs *discordgo.Session, c *utils.Config) error {
	statusChannel := strconv.FormatInt(categoryStatuses.Category.Metadata.StatusChannel, 10)
	message, err := dgs.ChannelMessageSendEmbed(statusChannel, embed)

	if err != nil {
		return err
	}

	// Handle errors here
	saveErr := saveMessage(message, c, categoryStatuses)

	if saveErr != nil {
		return saveErr.Err
	}

	return nil
}

func generateEmbed(categoryStatuses *CategoryStatuses, c *utils.Config) *discordgo.MessageEmbed {
	embed := &discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Updated",
		},
		Color:       0x4A90E2, // Blue
		Description: categoryStatuses.Category.Metadata.Description,
		Fields:      []*discordgo.MessageEmbedField{},
		Timestamp:   time.Now().Format(time.RFC3339), // Discord wants ISO8601; RFC3339 is an extension of ISO8601 and should be completely compatible.
		Title:       categoryStatuses.Category.Metadata.DisplayName,
		URL:         c.SourceCode,
	}

	if categoryStatuses.IconURL != "" {
		embed.Thumbnail = &discordgo.MessageEmbedThumbnail{
			URL: categoryStatuses.IconURL,
		}
	}

	for _, status := range categoryStatuses.Statuses {
		field := &discordgo.MessageEmbedField{
			Name:   "\u200b",
			Value:  formatStatusInfo(status),
			Inline: false,
		}

		embed.Fields = append(embed.Fields, field)
	}

	return embed
}

func getStatusIcon(statusText string) string {
	switch statusText {
	case "started":
		return "🟢"
	case "stopping":
	case "restarting":
		return "🟠"
	case "stopped":
	case "suspended":
		return "🔴"
	default:
		return "🔴"
	}

	return "🔴"
}

func formatStatusInfo(status Status) string {
	currentTime := time.Now().Unix()

	var daysUntilSuspend int64

	if status.SuspendDate != 0 {
		timeUntilSuspend := status.SuspendDate - currentTime
		daysUntilSuspend = int64(math.Floor(float64(timeUntilSuspend) / 86400.0))
	}

	var statusInfo string

	serviceNameAndStatus := getStatusIcon(status.Status) + " " + status.DisplayName

	statusInfo = fmt.Sprintf("**%s**\n", serviceNameAndStatus)

	if status.PlayersMax != 0 {
		statusInfo = fmt.Sprintf("%s*Players*: %d/%d\n", statusInfo, status.PlayersCurrent, status.PlayersMax)
	}

	if status.BoostCode != "" {
		statusInfo = fmt.Sprintf("%s*Boost*: %s\n", statusInfo, status.BoostCode)
	}

	if status.SuspendDate != 0 && status.ShowHealth {
		statusInfo = fmt.Sprintf("%s*Health*: %d days\n", statusInfo, daysUntilSuspend)
	}

	return statusInfo
}

func getMessage(categoryStatuses *CategoryStatuses, c *utils.Config) (*models.StatusMessage, *utils.ModelError) {
	dbInterface, err := db.GetDB(c)

	if err != nil {
		return nil, err
	}

	defer dbInterface.GetDB().Close()

	statusMessage := models.StatusMessage{
		DiscordGuild:   categoryStatuses.GuildID,
		DiscordChannel: categoryStatuses.Category.Metadata.StatusChannel,
		Category:       categoryStatuses.Category.CategoryName,
	}

	dbStatusMessage, dbErr := statusMessage.GetLatest(dbInterface)

	if dbErr != nil {
		return nil, dbErr
	}

	return &dbStatusMessage, nil
}

func saveMessage(message *discordgo.Message, c *utils.Config, categoryStatuses *CategoryStatuses) *utils.ModelError {
	dbInterface, err := db.GetDB(c)

	if err != nil {
		return err
	}

	defer dbInterface.GetDB().Close()

	messageID, _ := strconv.ParseInt(message.ID, 10, 64)

	statusMessage := models.StatusMessage{
		DiscordGuild:     categoryStatuses.GuildID,
		DiscordChannel:   categoryStatuses.Category.Metadata.StatusChannel,
		DiscordMessageID: messageID,
		Category:         categoryStatuses.Category.CategoryName,
	}

	dbErr := statusMessage.Update(dbInterface)

	return dbErr
}
