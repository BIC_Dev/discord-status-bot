module gitlab.com/BIC_Dev/discord-status-bot

go 1.14

require (
	github.com/bwmarrin/discordgo v0.21.1
	gopkg.in/yaml.v2 v2.3.0
)
