package models

import (
	"fmt"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/discord-status-bot/utils"
	"gitlab.com/BIC_Dev/discord-status-bot/utils/db"
)

// StatusMessage model
type StatusMessage struct {
	gorm.Model
	DiscordGuild     int64
	DiscordChannel   int64
	DiscordMessageID int64
	Category         string `gorm:"varchar(50)"`
}

// TableName func
func (StatusMessage) TableName() string {
	return "status_message"
}

// Create adds a record to DB
func (t *StatusMessage) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create status message entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GameserverID
func (t *StatusMessage) GetLatest(DBStruct db.Interface) (StatusMessage, *utils.ModelError) {
	var statusMessage StatusMessage

	result := DBStruct.GetDB().Where("discord_guild = ? AND discord_channel = ? AND category = ?", t.DiscordGuild, t.DiscordChannel, t.Category).Last(&statusMessage)

	if gorm.IsRecordNotFoundError(result.Error) {
		return statusMessage, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find ark status entry by gameserver_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return StatusMessage{}, modelError
	}

	return statusMessage, nil
}

// Update updates a record or creates it if it doesn't exist
func (t *StatusMessage) Update(DBStruct db.Interface) *utils.ModelError {
	getResult, getErr := t.GetLatest(DBStruct)

	if getErr != nil {
		return getErr
	}

	if getResult.ID == 0 {
		createErr := t.Create(DBStruct)

		if createErr != nil {
			return createErr
		}

		return nil
	}

	result := DBStruct.GetDB().Model(&t).Where("discord_guild = ? AND discord_channel = ? AND category = ?", t.DiscordGuild, t.DiscordChannel, t.Category).Updates(map[string]interface{}{
		"discord_message_id": t.DiscordMessageID,
	})

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage(fmt.Sprintf("Unable to update status entry in DB: %+v", &t))
		modelError.SetStatus(http.StatusInternalServerError)
		return modelError
	}

	return nil
}
