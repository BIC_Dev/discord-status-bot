package runners

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/BIC_Dev/discord-status-bot/services/nitrado"
	"gitlab.com/BIC_Dev/discord-status-bot/utils"
)

// GetServices func
func (r *Runner) GetServices(category utils.Category, e chan *utils.ApplicationError) {
	r.Log.Log(fmt.Sprintf("PROCESS: Getting services for: %s", category.CategoryName), r.Log.LogInformation)

	request := nitrado.Request{
		Config: r.Config,
		Log:    r.Log,
	}

	for _, service := range category.Services {
		_, err := request.GetService(r.NitradoServiceToken, strconv.FormatInt(service.ServiceID, 10), service.NitradoTokenID)

		if err != nil {
			applicationErr := utils.NewApplicationError(err)
			applicationErr.SetMessage(fmt.Sprintf("Failed to get service for: %d", service.ServiceID))
			applicationErr.SetStatus(http.StatusInternalServerError)
			e <- applicationErr
			continue
		}
	}

	return
}
