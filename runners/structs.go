package runners

import "gitlab.com/BIC_Dev/discord-status-bot/utils"

// Runner struct
type Runner struct {
	Config              *utils.Config
	Log                 *utils.Log
	NitradoServiceToken string
}

// CategoryStatuses struct
type CategoryStatuses struct {
	GuildID  int64
	Category utils.Category
	IconURL  string
	Statuses []Status
}

// Status struct
type Status struct {
	ServiceID      int
	Status         string
	PlayersCurrent int
	PlayersMax     int
	SuspendDate    int64
	DeleteDate     int64
	DisplayName    string
	RoleID         int64
	PlayerID       int64
	BoostCode      string
	ShowHealth     bool
}
