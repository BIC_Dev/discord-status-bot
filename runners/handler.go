package runners

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/discord-status-bot/services/discord"
	"gitlab.com/BIC_Dev/discord-status-bot/utils"
)

// StatusRunner func
func (r *Runner) StatusRunner(dgs *discordgo.Session) {
	r.Log.Log("PROCESS: Status runner started", r.Log.LogInformation)

	ticker := time.NewTicker(time.Duration(r.Config.Runners.StatusInterval) * time.Second)
	var categoryStatuses chan *CategoryStatuses = make(chan *CategoryStatuses, 10)
	var errors chan *utils.ApplicationError = make(chan *utils.ApplicationError, 50)

	go r.ErrorHandler(errors)
	go r.PostStatus(dgs, categoryStatuses)

	for range ticker.C {
		for _, guild := range r.Config.Guilds {
			for _, category := range guild.Categories {
				r.Log.Log(fmt.Sprintf("PROCESS: Running status for: %s", category.CategoryName), r.Log.LogInformation)
				go r.GetStatus(guild.GuildID, category, categoryStatuses, errors)
			}
		}

	}
}

// ServiceRunner func
func (r *Runner) ServiceRunner(dgs *discordgo.Session) {
	r.Log.Log("PROCESS: Service runner started", r.Log.LogInformation)

	ticker := time.NewTicker(time.Duration(r.Config.Runners.ServicesInterval) * time.Second)
	var errors chan *utils.ApplicationError = make(chan *utils.ApplicationError, 50)

	go r.ErrorHandler(errors)

	for range ticker.C {
		for _, guild := range r.Config.Guilds {
			for _, category := range guild.Categories {
				r.Log.Log(fmt.Sprintf("PROCESS: Running services for: %s", category.CategoryName), r.Log.LogInformation)
				go r.GetServices(category, errors)
			}
		}

	}
}

// PostStatus func
func (r *Runner) PostStatus(dgs *discordgo.Session, categoryStatuses chan *CategoryStatuses) {
	for {
		categoryStatus := <-categoryStatuses

		var statuses []discord.Status

		for _, v := range categoryStatus.Statuses {
			statuses = append(statuses, discord.Status(v))
		}

		cs := discord.CategoryStatuses{
			GuildID:  categoryStatus.GuildID,
			Category: categoryStatus.Category,
			IconURL:  categoryStatus.IconURL,
			Statuses: statuses,
		}

		err := discord.PostStatus(&cs, dgs, r.Config)

		if err != nil {
			r.Log.Log(fmt.Sprintf("ERROR: Failed to post status update: %s", err.Error()), r.Log.LogHigh)
			continue
		}

		r.Log.Log(fmt.Sprintf("SUCCESS: Posted status to Discord for %s on channel: %d", categoryStatus.Category.CategoryName, categoryStatus.Category.Metadata.StatusChannel), r.Log.LogInformation)
	}
}
