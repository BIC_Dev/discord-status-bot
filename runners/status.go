package runners

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/BIC_Dev/discord-status-bot/services/nitrado"
	"gitlab.com/BIC_Dev/discord-status-bot/utils"
)

// GetStatus func
func (r *Runner) GetStatus(guildID int64, category utils.Category, s chan *CategoryStatuses, e chan *utils.ApplicationError) {
	r.Log.Log(fmt.Sprintf("PROCESS: Getting status for: %s", category.CategoryName), r.Log.LogInformation)

	var services []utils.Service

	for _, service := range category.Services {
		services = append(services, service)
	}

	if len(services) < 1 {
		applicationErr := utils.NewApplicationError(fmt.Errorf("Failed to find services for category: %s", category.CategoryName))
		applicationErr.SetMessage(fmt.Sprintf("Nitrado config element is missing services for the %s category", category.CategoryName))
		applicationErr.SetStatus(http.StatusInternalServerError)
		e <- applicationErr

		return
	}

	request := nitrado.Request{
		Config: r.Config,
		Log:    r.Log,
	}

	categoryStatuses := CategoryStatuses{
		GuildID:  guildID,
		Category: category,
		IconURL:  category.Metadata.IconURL,
	}

	for _, service := range services {
		gameserver, err := request.GetServerStatus(r.NitradoServiceToken, strconv.FormatInt(service.ServiceID, 10), service.NitradoTokenID)

		if err != nil {
			applicationErr := utils.NewApplicationError(err)
			applicationErr.SetMessage(fmt.Sprintf("Failed to get gameserver status for: %d", service.ServiceID))
			applicationErr.SetStatus(http.StatusInternalServerError)
			e <- applicationErr
			continue
		}

		status := Status{
			ServiceID:      gameserver.Gameserver.ServiceID,
			Status:         gameserver.Gameserver.Status,
			PlayersCurrent: gameserver.Gameserver.Query.PlayerCurrent,
			PlayersMax:     gameserver.Gameserver.Query.PlayerMax,
			SuspendDate:    gameserver.Gameserver.SuspendDate,
			DeleteDate:     gameserver.Gameserver.DeleteDate,
			DisplayName:    service.DisplayName,
			RoleID:         service.RoleID,
			PlayerID:       service.PlayerID,
			BoostCode:      service.BoostCode,
			ShowHealth:     category.Metadata.ShowHealth,
		}

		categoryStatuses.Statuses = append(categoryStatuses.Statuses, status)
	}

	s <- &categoryStatuses
	return
}
