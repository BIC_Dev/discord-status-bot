package runners

import (
	"fmt"

	"gitlab.com/BIC_Dev/discord-status-bot/utils"
)

// ErrorHandler func
func (r *Runner) ErrorHandler(e chan *utils.ApplicationError) {
	for {
		err := <-e
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", err.GetMessage(), err.Error()), r.Log.LogLow)
	}
}
