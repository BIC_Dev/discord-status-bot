package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/discord-status-bot/controllers"
	"gitlab.com/BIC_Dev/discord-status-bot/models"
	"gitlab.com/BIC_Dev/discord-status-bot/routes"
	"gitlab.com/BIC_Dev/discord-status-bot/runners"
	"gitlab.com/BIC_Dev/discord-status-bot/utils"
	"gitlab.com/BIC_Dev/discord-status-bot/utils/db"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *utils.Log
	DG     *discordgo.Session
}

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	service := Service{
		Config: utils.GetConfig(env),
		Log:    utils.InitLog(logLevel),
	}

	if os.Getenv("MIGRATE") == "TRUE" {
		service.migrateTables(models.StatusMessage{})
	}

	discordToken := os.Getenv("DISCORD_TOKEN")

	dg, discErr := discordgo.New("Bot " + discordToken)

	if discErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Error creating Discord session: %s", discErr.Error()), service.Log.LogFatal)
		return
	}

	service.DG = dg

	// Register the messageCreate func as a callback for MessageCreate events.
	//dg.AddHandler(messageCreate)

	// Open a websocket connection to Discord and begin listening.
	openErr := dg.Open()
	if openErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Error opening connection: %s", openErr.Error()), service.Log.LogFatal)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	service.Log.Log("PROCESS: Bot is now running", service.Log.LogInformation)

	service.Log.Log("PROCESS: Starting runners", service.Log.LogInformation)

	runner := runners.Runner{
		Config:              service.Config,
		Log:                 service.Log,
		NitradoServiceToken: os.Getenv("NITRADO_SERVICE_TOKEN"),
	}

	go runner.StatusRunner(dg)
	go runner.ServiceRunner(dg)

	controller := controllers.Controller{
		Config:       service.Config,
		Log:          service.Log,
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
	}

	router := routes.GetRouter()
	routes.AddRoutes(router, &controller)

	service.Log.Log("SUCCESS: Set up router", service.Log.LogInformation)

	service.Log.Log("PROCESS: Starting MUX listener", service.Log.LogInformation)
	routes.StartListener(router, os.Getenv("LISTENER_PORT"))

	// Cleanly close down the Discord session.
	dg.Close()
}

func (s *Service) migrateTables(tables ...interface{}) []*utils.ModelError {
	s.Log.Log("Migrating tables: status_message", s.Log.LogInformation)
	var migrationErrors []*utils.ModelError

	dbStruct, dbErr := db.GetDB(s.Config)

	if dbErr != nil {
		s.Log.Log(fmt.Sprintf("%s: %s", dbErr.GetMessage(), dbErr.Error()), s.Log.LogFatal)
	}

	defer dbStruct.GetDB().Close()

	for _, table := range tables {
		migrationError := dbStruct.Migrate(table)

		if migrationError != nil {
			migrationErrors = append(migrationErrors, migrationError)
		}
	}

	if len(migrationErrors) > 0 {
		return migrationErrors
	}

	return nil
}
