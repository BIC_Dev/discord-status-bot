package utils

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	DB struct {
		Type string `yaml:"type"`
	} `yaml:"DB"`
	SQLite3 struct {
		Path   string `yaml:"path"`
		DBName string `yaml:"db_name"`
	} `yaml:"SQLITE3"`
	PostgreSQL struct {
		Host   string `yaml:"host"`
		Port   string `yaml:"port"`
		DBName string `yaml:"db_name"`
	} `yaml:"POSTGRESQL"`
	SourceCode string `yaml:"SOURCE_CODE"`
	Runners    struct {
		StatusInterval   int `yaml:"status_interval"`
		ServicesInterval int `yaml:"services_interval"`
	} `yaml:"RUNNERS"`
	Guilds         []Guild        `yaml:"GUILDS"`
	NitradoService NitradoService `yaml:"NITRADO_SERVICE"`
}

// Guild struct
type Guild struct {
	GuildID    int64      `yaml:"guild_id"`
	Categories []Category `yaml:"categories"`
}

// Category struct
type Category struct {
	CategoryName string    `yaml:"category_name"`
	Metadata     Metadata  `yaml:"metadata"`
	Services     []Service `yaml:"services"`
}

// Metadata struct
type Metadata struct {
	Username      string `yaml:"username"`
	StatusChannel int64  `yaml:"status_channel"`
	DisplayName   string `yaml:"display_name"`
	Description   string `yaml:"description"`
	IconURL       string `yaml:"icon_url"`
	ShowHealth    bool   `yaml:"show_health"`
}

// Service struct
type Service struct {
	ServiceID      int64  `yaml:"service_id"`
	NitradoTokenID string `yaml:"nitrado_token_id"`
	DisplayName    string `yaml:"display_name"`
	RoleID         int64  `yaml:"role_id"`
	PlayerID       int64  `yaml:"player_id"`
	BoostCode      string `yaml:"boost_code"`
}

// NitradoService struct
type NitradoService struct {
	BaseURL string `yaml:"base_url"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(env string) *Config {
	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		log.Fatal("Missing config file at path: " + configFile)
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		log.Fatal("Could not parse config file")
	}

	return &config
}
